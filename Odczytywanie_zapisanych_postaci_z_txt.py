#!/usr/bin/env python
# coding: utf-8

# In[9]:


import tkinter as tk
from tkinter import *
from tkinter import simpledialog
from tkinter import messagebox
from tkinter import filedialog
import os

klasy = {
    "Wojownik":"siła",
    "Łotrzyk":"zwinność",
    "Czarodziej":"inteligencja",
    "Kapłan":"charyzma",
    "Awanturnik":None
}
rasy = {
    "Krasnolud":"zwinność",
    "Elf":"siła",
    "Niziołek":"inteligencja",
    "Ork":"charyzma",
    "Człowiek":None
}

photoimage_list = []

def otwieraniezapisanejpostaci():
    filename =  filedialog.askopenfilename(initialdir = "/",title = "Select file",filetypes = (("txt files","*.txt"),("all files","*.*")))
    imie = os.path.basename(filename)[:4]
    tex3.insert(INSERT, imie + "\n")
    
    with open(filename, 'r') as f:
        tex2.insert(INSERT, f.read())
    
    for key, value in klasy.items():
        if key in open(filename,'r').read():
            photo=tk.PhotoImage(file=key+'.gif')
            photoimage_list.append(photo)
            label = tk.Label(top,image=photo,justify=tk.LEFT)
            label.pack()
    
    for key, value in rasy.items():
        if key in open(filename,'r').read():
            photo2=tk.PhotoImage(file=key+'.gif')
            photoimage_list.append(photo2)
            label = tk.Label(top,image=photo2,justify=tk.RIGHT)
            label.pack()
            
    
            
def edytuj_otwarte():
    t = tk.Toplevel(top)
    przedmioty = tk.Label(t, text="Przedmioty")
    przedmioty.pack()
    przedmiotywpisz = tk.Entry(t)
    przedmiotywpisz.pack()
    złoto = tk.Label(t, text="Złoto")
    złoto.pack()
    złotowpisz = tk.Entry(t)
    złotowpisz.pack()
    edytuj=tk.Button(t, text="Edytuj", command=lambda: dodajtekst(przedmiotywpisz.get(),złotowpisz.get()))
    edytuj.pack()
    
def dodajtekst(pierwszy,drugi):
    s1 = "Przedmioty: " + pierwszy + "\n"
    s2 = "Złoto: " + drugi + "\n"
    tex2.insert(INSERT, s1)
    tex2.insert(INSERT, s2)
    
def zapispostaci():
    postacedytowana=tex2.get("1.0", END)
    print(tex3.get("1.0", END))
    imie = tex3.get("1.0", END)
    imie2 = imie.replace('\n',"")
    file=open(imie2+".txt","w")
    file.write(str(postacedytowana))
    file.close()
            
top = tk.Tk()
'''
tex1 = tk.Text(master=top,height=20, width=55)
tex1.pack(side=tk.RIGHT)
'''
tex2 = tk.Text(master=top,height=20, width=50)
tex2.pack(side=tk.RIGHT)

tex3 = tk.Text(master=top,height=1,width=9)
tex3.config(font=("Helvetica", 20, 'bold'))
tex3.pack(side=tk.TOP)
    
bop = tk.Frame()
bop.pack(side=tk.LEFT)

ozapis = tk.Button(bop, text='Otwórz zapisaną postać',command= lambda: otwieraniezapisanejpostaci())
ozapis.pack()

edycja = tk.Button(bop, text='Edytuj postać', command=lambda: edytuj_otwarte())
edycja.pack()

zzapis = tk.Button(bop, text='Zapisz zedytowaną postać', command= lambda: zapispostaci())
zzapis.pack()


tk.Button(bop, text='Exit', command=top.destroy).pack()
top.mainloop()


# In[ ]:





# In[ ]:





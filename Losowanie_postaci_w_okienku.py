#!/usr/bin/env python
# coding: utf-8

# In[7]:


import tkinter as tk
from random import randint
from tkinter import *
from tkinter import simpledialog
from tkinter import messagebox

#nazwy 
atrybuty = ("siła","zwinność","inteligencja","charyzma","zdrowie","mana")

klasy = {
    "Wojownik":"siła",
    "Łotrzyk":"zwinność",
    "Czarodziej":"inteligencja",
    "Kapłan":"charyzma",
    "Awanturnik":None
}

rasy = {
    "Krasnolud":"zwinność",
    "Elf":"siła",
    "Niziołek":"inteligencja",
    "Ork":"charyzma",
    "Człowiek":None
}

#misje
odwiedz = {
    1:"Miasto występku",
    2:"Rajską Wyspę",
    3:"Plugawe trzęsawiska",
    4:"Jałową pustynię",
    5:"Mroczną puszczę",
    6:"Mroźne góry"
}
gdzie_grasuje = {
    1:"Golem",
    2:"Wikołak",
    3:"Wampir",
    4:"Gryf",
    5:"Wikołak",
    6:"Troll"
}
oraz = {
    1:"Koboldy",
    2:"Wilki",
    3:"Szkielety",
    4:"Impy",
    5:"Pająki",
    6:"Gobliny"
}
aby = {
    1:"Kupić",
    2:"Ukryć",
    3:"Sprzedać",
    4:"Znaleźć",
    5:"Zniszczyć",
    6:"Naprawić"
}
pewna = {
    1:"Dziwaczną machinę",
    2:"Pancerną skrzynię",
    3:"Tajemniczą księgę",
    4:"Mapę skarbu",
    5:"Magiczną istotę",
    6:"Rodzinną pamiątkę"
}
ktora = {
    1:"Skradziono",
    2:"Zatopiono",
    3:"Przeklęto",
    4:"Zgubiono",
    5:"Zakopano",
    6:"Podzielono"
}

#charakterystyka
sylwetka = {
    1:"Drobna",
    2:"Smukła",
    3:"Krągła",
    4:"Kanciasta",
    5:"Postawna",
    6:"Muskularna"
}
glos = {
    1:"Chrapliwy",
    2:"Cichy",
    3:"Wysoki",
    4:"Niski",
    5:"Melodyjny",
    6:"Donośny"
}
charakter = {
    1:"Ostrożny",
    2:"Troskliwy",
    3:"Odważny",
    4:"Pogodny",
    5:"Ambitny",
    6:"Stanowczy"
}
oblicze = {
    1:"Krosty",
    2:"Blizny",
    3:"Tatuaże",
    4:"Kolczyki",
    5:"Malunki",
    6:"Opalenizna"
}
wyznanie = {
    1:"Brat Bestia",
    2:"Siostra Woda",
    3:"Brat Ogień",
    4:"Żelazny Dziedzic",
    5:"Matka Ziemia",
    6:"Ojciec Niebo"
}
pochodzenie = {
    1:"Ciemne zaułki",
    2:"Portowe miasto",
    3:"Pustynna oaza",
    4:"Bagienna kryjówka",
    5:"Leśna wioska",
    6:"Górska twierdza"
}

def szesckostek():
    wyniki = list()
    for i in range(0,6):
        wyniki.append(randint(1,6))
    return wyniki

def najwieksza(wartosci):
    maxValue = max(wartosci.values())
    najwieksze = list()
    for key in wartosci: 
        if wartosci[key]==maxValue:
            najwieksze.append(key)
    return najwieksze

def najmniejsza(wartosci):
    minValue = min(wartosci.values())
    najmniejsze = list()
    for key in wartosci: 
        if wartosci[key]==minValue:
            najmniejsze.append(key)
    return najmniejsze

#wyniki losowania i połączone z wartościami
wyniki = szesckostek()
wartosci = dict(zip(atrybuty,wyniki))

def zapispostaci():
    postacstworzona=tex.get("1.0", END)
    imie = tk.simpledialog.askstring('Imie postaci','Nadaj imię postaci')
    file=open(imie+".txt","w")
    file.write(str(postacstworzona))
    file.close()
    
def c1(tex):
    return lambda : tekst(tex)

def tekst(tex):
    s = "Charyzma: " + str(wartosci.get('charyzma', None)) + "\n" + "Inteligencja: " + str(wartosci.get('inteligencja', None)) + "\n" + "Zwinność: " + str(wartosci.get('zwinność', None)) + "\n" + "Zdrowie: " + str(wartosci.get('zdrowie', None)) + "\n" + "Siła: " + str(wartosci.get('siła', None)) + "\n" + "Mana: " + str(wartosci.get('mana', None)) + "\n" 
    tex.insert(tk.END, s)
    tex.see(tk.END)             
    
def c2(tex):
    return lambda : tekstk(tex)

def tekstk(tex):
    wartosci.pop("zdrowie",None)
    wartosci.pop("mana",None)
    najwieksze = najwieksza(wartosci)
    if len(najwieksze)==1:
        for key, value in klasy.items():
            if str(najwieksze[0]) == value:
                s = "Klasa: " + key + "\n"
    if len(najwieksze)>1:
        wybor = tk.simpledialog.askstring('Wybór klasy', 'Wybierz jeden z atrybutów\n'+str(najwieksze))
        for key, value in klasy.items():
            if wybor == value:
                s = "Klasa: " + key + "\n"
    awanturnik = tk.messagebox.askyesno("Zmiana klasy", "Zmienić klasę na Awanturnika?")
    if awanturnik == True:
        s = "Klasa: Awanturnik\n"
    tex.insert(tk.END, s)
    tex.see(tk.END)             

def c3(tex):
    return lambda : tekstr(tex)

def tekstr(tex):
    wartosci.pop("zdrowie",None)
    wartosci.pop("mana",None)
    najmniejsze = najmniejsza(wartosci)
    if len(najmniejsze)==1:
        for key, value in rasy.items():
            if str(najmniejsze[0]) == value:
                s = "Rasa: " + key + "\n"
    if len(najmniejsze)>1:
        wybor = tk.simpledialog.askstring('Wybór rasy', 'Wybierz jeden z atrybutów\n'+str(najmniejsze))
        for key, value in rasy.items():
            if wybor == value:
                s = "Rasa: " + key + "\n"
    czlowiek = tk.messagebox.askyesno("Zmiana rasy", "Zmienić rasę na Człowieka?")
    if czlowiek == True:
        s = "Rasa: Człowiek\n"
    tex.insert(tk.END, s)
    tex.see(tk.END)             

def c4(tex):
    return lambda : tekstm(tex)

def tekstm(tex):
    misja=szesckostek()
    wylosowana_misja="Odwiedź"+" "+odwiedz[misja[0]]+" "+"gdzie grasuje"+" "+gdzie_grasuje[misja[1]].lower()+" "+"oraz"+" "+oraz[misja[2]].lower()+" "+"aby"+" "+aby[misja[3]].lower()+" "+"pewną"+" "+pewna[misja[4]].lower()+" "+"którą"+" "+ktora[misja[5]].lower()+"."
    s = wylosowana_misja + "\n"
    tex.insert(tk.END, s)
    tex.see(tk.END)

def c5(tex):
    return lambda : tekstch(tex)

def tekstch(tex):
    charakterystyka=szesckostek()
    wylosowana_charakterystyka="Sylwetka:"+" "+sylwetka[charakterystyka[0]].lower()+","+" "+"Głos:"+" "+glos[charakterystyka[1]].lower()+","+" "+"Charakter:"+" "+charakter[charakterystyka[2]].lower()+","+" "+"Oblicze:"+" "+oblicze[charakterystyka[3]].lower()+","+" "+"Wyznanie:"+" "+wyznanie[charakterystyka[4]]+","+" "+"Pochodzenie:"+" "+pochodzenie[charakterystyka[5]].lower()
    s = wylosowana_charakterystyka + "\n"
    tex.insert(tk.END, s)
    tex.see(tk.END)

top = tk.Tk()
tex = tk.Text(master=top)
tex.pack(side=tk.RIGHT)
bop = tk.Frame()
bop.pack(side=tk.LEFT)

atrybuty = tk.Button(bop, text='Losuj atrybuty', command=c1(tex))
atrybuty.pack()

klasa = tk.Button(bop, text='Losuj klasę', command=c2(tex))
klasa.pack()

rasa = tk.Button(bop, text='Losuj rasę', command=c3(tex))
rasa.pack()

misja = tk.Button(bop, text='Losuj misję', command=c4(tex))
misja.pack()

charakterystyka = tk.Button(bop, text='Losuj charakterystykę', command=c5(tex))
charakterystyka.pack()

zapis = tk.Button(bop, text='Zapisz postać',command =lambda: zapispostaci())
zapis.pack()

tk.Button(bop, text='Exit', command=top.destroy).pack()
top.mainloop()


# In[ ]:





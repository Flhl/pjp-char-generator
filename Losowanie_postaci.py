#!/usr/bin/env python
# coding: utf-8


from random import randint

def szesckostek():
    wyniki = list()
    for i in range(0,6):
        wyniki.append(randint(1,6))
    return wyniki

def najwieksza(wartosci):
    maxValue = max(wartosci.values())
    najwieksze = list()
    for key in wartosci: 
        if wartosci[key]==maxValue:
            najwieksze.append(key)
    return najwieksze

def najmniejsza(wartosci):
    minValue = min(wartosci.values())
    najmniejsze = list()
    for key in wartosci: 
        if wartosci[key]==minValue:
            najmniejsze.append(key)
    return najmniejsze
    
atrybuty = ("siła","zwinność","inteligencja","charyzma","zdrowie","mana")
rasy = {
    "Krasnolud":"zwinność",
    "Elf":"siła",
    "Niziołek":"inteligencja",
    "Ork":"charyzma",
    "Człowiek":None
}
klasy = {
    "Wojownik":"siła",
    "Łotrzyk":"zwinność",
    "Czarodziej":"inteligencja",
    "Kapłan":"charyzma",
    "Awanturnik":None
}

#if _name_ = "Losowanie postaci"
postac = list()
wyniki=szesckostek()
wartosci = dict(zip(atrybuty,wyniki))
print(wartosci)
postac.append(wartosci)
print(postac)

#losowanie klasy
najwieksze = najwieksza(wartosci)
if len(najwieksze)==1:
    for key, value in klasy.items():
        if str(najwieksze[0]) == value:
            postac.append(key)
            print(key)
if len(najwieksze)>1:
    print(najwieksze)
    wybor = str(input("Wybierz jeden z atrybutów: "))
    for key, value in klasy.items():
        if str(wybor) == value:
            postac.append(key)

#losowanie rasy
najmniejsze = najmniejsza(wartosci)
if len(najmniejsze)==1:
    for key, value in rasy.items():
        if str(najmniejsze[0]) == value:
            postac.append(key)
            print(key)
if len(najmniejsze)>1:
    print(najmniejsze)
    wybor = str(input("Wybierz jeden z atrybutów: "))
    for key, value in rasy.items():
        if str(wybor) == value:
            postac.append(key)
misja=szesckostek()

#misje
odwiedz = {
    1:"Miasto występku",
    2:"Rajską Wyspę",
    3:"Plugawe trzęsawiska",
    4:"Jałową pustynię",
    5:"Mroczną puszczę",
    6:"Mroźne góry"
}
gdzie_grasuje = {
    1:"Golem",
    2:"Wikołak",
    3:"Wampir",
    4:"Gryf",
    5:"Wikołak",
    6:"Troll"
}
oraz = {
    1:"Koboldy",
    2:"Wilki",
    3:"Szkielety",
    4:"Impy",
    5:"Pająki",
    6:"Gobliny"
}
aby = {
    1:"Kupić",
    2:"Ukryć",
    3:"Sprzedać",
    4:"Znaleźć",
    5:"Zniszczyć",
    6:"Naprawić"
}
pewna = {
    1:"Dziwaczną machinę",
    2:"Pancerną skrzynię",
    3:"Tajemniczą księgę",
    4:"Mapę skarbu",
    5:"Magiczną istotę",
    6:"Rodzinną pamiątkę"
}
ktora = {
    1:"Skradziono",
    2:"Zatopiono",
    3:"Przeklęto",
    4:"Zgubiono",
    5:"Zakopano",
    6:"Podzielono"
}

wylosowana_misja="Odwiedź"+" "+odwiedz[misja[0]]+" "+"gdzie grasuje"+" "+gdzie_grasuje[misja[1]].lower()+" "+"oraz"+" "+oraz[misja[2]].lower()+" "+"aby"+" "+aby[misja[3]].lower()+" "+"pewną"+" "+pewna[misja[4]].lower()+" "+"którą"+" "+ktora[misja[5]].lower()+"."
postac.append(wylosowana_misja)
print(postac)

charakterystyka=szesckostek()

#charakterystyka
sylwetka = {
    1:"Drobna",
    2:"Smukła",
    3:"Krągła",
    4:"Kanciasta",
    5:"Postawna",
    6:"Muskularna"
}
glos = {
    1:"Chrapliwy",
    2:"Cichy",
    3:"Wysoki",
    4:"Niski",
    5:"Melodyjny",
    6:"Donośny"
}
charakter = {
    1:"Ostrożny",
    2:"Troskliwy",
    3:"Odważny",
    4:"Pogodny",
    5:"Ambitny",
    6:"Stanowczy"
}
oblicze = {
    1:"Krosty",
    2:"Blizny",
    3:"Tatuaże",
    4:"Kolczyki",
    5:"Malunki",
    6:"Opalenizna"
}
wyznanie = {
    1:"Brat Bestia",
    2:"Siostra Woda",
    3:"Brat Ogień",
    4:"Żelazny Dziedzic",
    5:"Matka Ziemia",
    6:"Ojciec Niebo"
}
pochodzenie = {
    1:"Ciemne zaułki",
    2:"Portowe miasto",
    3:"Pustynna oaza",
    4:"Bagienna kryjówka",
    5:"Leśna wioska",
    6:"Górska twierdza"
}

wylosowana_charakterystyka="Sylwetka:"+" "+sylwetka[charakterystyka[0]].lower()+","+" "+"Głos:"+" "+glos[charakterystyka[1]].lower()+","+" "+"Charakter:"+" "+charakter[charakterystyka[2]].lower()+","+" "+"Oblicze:"+" "+oblicze[charakterystyka[3]].lower()+","+" "+"Wyznanie:"+" "+wyznanie[charakterystyka[4]]+","+" "+"Pochodzenie:"+" "+pochodzenie[charakterystyka[5]].lower()
postac.append(wylosowana_charakterystyka)
print(postac)

"""
from tkinter import *


class Window(Frame):


    def __init__(self, master=None):
        Frame.__init__(self, master)                 
        self.master = master
        self.init_window()
        
    def init_window(self):
        self.master.title("GUI")
        self.pack(fill=BOTH, expand=1)
        oneButton = Button(self, text="Stwórz postać",)
        oneButton.place(x=50, y=150)
        twoButton = Button(self, text="Zapisane postacie")
        twoButton.place(x=150, y=150)
        quitButton = Button(self, text="Wyjdź")
        quitButton.place(x=265, y=150)
        
    
root = Tk()
root.geometry("700x400")

app = Window(root)
root.mainloop()  

import tkinter
import cv2
import PIL.Image, PIL.ImageTk
window = tkinter.Tk()
window.title("OpenCV and Tkinter")

cv_img = cv2.cvtColor(cv2.imread("fjr rasa.png"), cv2.COLOR_BGR2RGB)
height, width, no_channels = cv_img.shape
 
canvas = tkinter.Canvas(window, width = width, height = height)
canvas.pack()

photo = PIL.ImageTk.PhotoImage(image = PIL.Image.fromarray(cv_img))
canvas.create_image(0, 0, image=photo, anchor=tkinter.NW)

window.mainloop()
"""